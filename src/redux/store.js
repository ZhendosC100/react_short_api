import { configureStore } from "@reduxjs/toolkit";
import productsReducer from './getProducts/getProducts'; 

export default configureStore({
  reducer: {
    products: productsReducer
  }
})