import { createSlice } from "@reduxjs/toolkit";

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: []
  },

  reducers: {
    getProducts(state) {

    }
  }
})

export const { getProducts } = productsSlice.actions;
export default productsSlice.reducer;
