import { ThemeProvider } from 'styled-components';
import GlobalStyle from './styles/GlobalStyle';
import { theme } from './styles/theme';
import { Header } from './components/'


const App = () => (

  <ThemeProvider theme={ theme }>
    <GlobalStyle />
    <Header />
  </ThemeProvider>

)



export default App;
