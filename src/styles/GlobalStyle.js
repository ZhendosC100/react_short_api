import { createGlobalStyle } from "styled-components";
import { normalize } from 'styled-normalize'

const GlobalStyle = createGlobalStyle`
 ${ normalize }

  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  html {
    font-size: 62.5%;
  }
`

export default GlobalStyle;